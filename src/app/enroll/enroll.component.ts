import { Component, OnInit } from '@angular/core';
import { Message }    from '../message';
import { FormService }    from '../form.service';


@Component({
  selector: 'app-enroll',
  templateUrl: './enroll.component.html',
  styleUrls: ['./enroll.component.scss']
})
export class EnrollComponent implements OnInit {

  public model: Message;
  private formService: FormService;
  public submitted: boolean = false;
  public submitError: string = '';
  public sending: boolean = false;

  constructor(formService: FormService) {
    this.formService = formService;
    this.model = new Message();
  }

  ngOnInit() {
  }

  handleSubmit() {
    console.log('Boton');
    this.sending = true;
    this.formService.sendMessage(this.model).subscribe(
			result => {

				if(result.code != 200){
					console.log(result);
          this.submitError = 'Error al enviar el formulario';
          this.sending = false;
				}else{
					console.log(result);
          this.submitted = true;
          this.submitError = '';
				}

			},
			error => {
				console.log(<any>error);
        this.submitError = 'Error al enviar el formulario';
        this.sending = false;
			}
		);


  }

}
