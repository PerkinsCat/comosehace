import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Message }    from './message';

@Injectable()
export class FormService {
  private url: string;
  constructor(public http: HttpClient) {
    this.url = '/api/';
  }

  sendMessage(message: Message): Observable<any>{
  		let json = JSON.stringify(message);
      console.log(json);

  //El backend recogerá un parametro json
  		let params = json;

                  //Establecemos cabeceras
  		let headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  		return this.http.post(this.url+'message', params, {headers: headers});
  	}


}
