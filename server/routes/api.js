const express = require('express');
const router = express.Router();
const nodemailer = require('nodemailer');

// declare axios for making http requests
//const axios = require('axios');
//const API = 'https://jsonplaceholder.typicode.com';

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

// Get all posts
router.post('/message', sendMail);


function sendMail(req, res){
  console.log(req.body);

  if(req.body.name && req.body.comments && req.body.email){
    let message = req.body;
    nodemailer.createTestAccount(() => {

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: 'mail.comosehace.es',
            port: 587,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'info@comosehace.es', // generated ethereal user
                pass: '3Kuzy_69'  // generated ethereal password
            },
            tls: {rejectUnauthorized: false}
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"Formulario de contacto" ' + message.email, // sender address
            to: 'info@comosehace.es', // list of receivers
            subject: 'Formulario de contacto: '+message.name, // Subject line
            text: message.comments.split('\n').join('<br>'), // plain text body
            html: message.comments.split('\n').join('<br>') // html body
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) {
                return console.log(error);
                res.status(500).json({error: 'Se ha producido un error al enviar el mensaje'});
            }
            console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
            console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
            res.status(200).json({code: 200});

        });
    });


  }else{
    res.status(500).json({error: 'Formulario vacio'});
  }

}

module.exports = router;
